;; -*-  mode: emacs-lisp; indent-tabs-mode: nil -*-

;;; Setup ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Increase GC threshold to improve startup time
(setq gc-cons-threshold (* 50 1000 1000))

(setq custom-file "~/.emacs-custom.el")
(load custom-file 'noerror)

;; Package mgmt
(require 'package)
(setq package-enable-at-startup nil)
(setq network-security-level 'high)
(add-to-list 'package-archives '("gnu" . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;;; Generic ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-auto-revert-mode 1)
(global-set-key (kbd "C-h C-c") 'describe-key-briefly)
(setq echo-keystrokes 0.02)
(setq org-src-tab-acts-natively t)

(setq frame-title-format
      '("" invocation-name " - "
        (:eval (if (buffer-file-name)
                   (abbreviate-file-name (buffer-file-name))
                 "%b"))))

(define-key minibuffer-local-map
  (kbd "<escape>") 'keyboard-escape-quit)
(define-key minibuffer-local-ns-map
  (kbd "<escape>") 'keyboard-escape-quit)
(define-key minibuffer-local-completion-map
  (kbd "<escape>") 'keyboard-escape-quit)
(define-key minibuffer-local-must-match-map
  (kbd "<escape>") 'keyboard-escape-quit)
(define-key minibuffer-local-isearch-map
  (kbd "<escape>") 'keyboard-escape-quit)

(setq
 scroll-margin 0
 scroll-conservatively 100000
 scroll-preserve-screen-position 1)

(setq
 column-number-mode t
 echo-keystrokes 0.02
 inhibit-startup-screen t)

(setq
 calendar-week-start-day 1)

(setq
 backup-by-copying t
 backup-directory-alist '(("." . "~/.saves"))
 delete-old-versions t
 kept-new-versions 6
 kept-old-versions 2
 version-control t)

(set-frame-parameter (selected-frame) 'alpha '(95 . 70))
(add-to-list 'default-frame-alist '(alpha . (95 . 70)))

(if (display-graphic-p)
    (global-hl-line-mode 1))


(setq initial-frame-alist
      '((width . 102)   ; characters in a line
        (height . 54))) ; number of lines

(setq default-frame-alist
      '((width . 100)   ; characters in a line
        (height . 52))) ; number of lines

(prefer-coding-system 'utf-8)
(fset 'yes-or-no-p 'y-or-n-p)

(add-to-list 'auto-mode-alist '("/mutt" . mail-mode))

(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;; C++

(add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
(c-add-style "cpp-ortcc"
             '("k&r"
               (c-basic-offset . 4)
               (tab-width . 4)
               (indent-tabs-mode . t)
               (c-offsets-alist
                (innamespace . 0)
                (inline-open . 0)
                (brace-list-entry . +)
                (substatement-open . 0)
                (member-init-intro . 0))))
(setq c-default-style "cpp-ortcc")

;; Enable ansi colors in compilation buffer
(require 'ansi-color)
(defun colorize-compilation-buffer ()
  (toggle-read-only)
  (ansi-color-apply-on-region compilation-filter-start (point))
  (toggle-read-only))
(add-hook 'compilation-filter-hook 'colorize-compilation-buffer)

;;; Themes ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package doom-themes :ensure t)
(use-package monokai-theme :ensure t)
(use-package darktooth-theme :ensure t)
(use-package atom-one-dark-theme :ensure t)
(use-package sublime-themes :ensure t)
(use-package gruvbox-theme :ensure t)
(use-package zenburn-theme :ensure t)

;;(if (display-graphic-p)
;;    (load-theme 'zenburn t)
;;    ;;(load-theme 'atom-one-dark t)
;;    ;;(load-theme 'dracula t)
;;    ;;(load-theme 'wilson t)
;;    ;;(load-theme 'gruvbox-dark-medium t)
;;  (load-theme 'wheatgrass t))

(defun r2-init ()
  "setup things when using window system"
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (scroll-bar-mode -1)
  (fringe-mode '(10 . 10))
  (global-visual-line-mode)
  (load-theme 'zenburn t))

(if (daemonp)
    (add-hook 'after-make-frame-functions
              (lambda (frame)
                (r2-init)))
  (r2-init))

;; Font
(add-to-list 'default-frame-alist '(font . "Source Code Pro 8"))
;;(add-to-list 'default-frame-alist '(font . "Operator Mono Light 8"))
;;(add-to-list 'default-frame-alist '(font . "ProggyCleanTT"))

;;; Packages ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(use-package diminish
  :ensure t)

(use-package powerline
  :ensure t
  :defer t)

(use-package spaceline
  :ensure t
  :demand t
  :init
  (setq powerline-default-separator 'arrow-fade)
  :config
  (require 'spaceline-config)
  (spaceline-spacemacs-theme)
  (spaceline-helm-mode)
  (spaceline-toggle-minor-modes-off))

(use-package fancy-battery
  :ensure t
  :defer t
  :init
  (setq fancy-battery-show-percentage t)
  :config
  (fancy-battery-mode))

;(use-package spaceline-all-the-icons
;  :ensure t
;  :after spaceline
;  :config (spaceline-all-the-icons-theme))

(use-package evil
  :ensure t
  :defer t
  :init
  (evil-mode))

(use-package magit
  :defer t
  :init
  (use-package evil-magit :ensure t)
  :config)

(use-package whitespace
  :defer 1
  :ensure nil ; built-in package
  :diminish whitespace-mode
  :config
  (setq-default whitespace-line-column 80)
  (setq-default whitespace-style '(face tab-mark trailing lines-tail newline))
  (setq-default whitespace-display-mappings
                '((newline-mark 10 [172 10])
                  (tab-mark 9 [8594 9])))
  (add-hook 'prog-mode-hook #'whitespace-mode))

(use-package cmake-mode
  :ensure t
  :defer t)

(use-package irony
  :ensure t
  :defer t
  :diminish irony-mode
  :init
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'objc-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options))

(use-package company
  :ensure t
  :defer 2
  :diminish company-mode
  :config
  (use-package company-irony
    :ensure t
    :defer t)
  :init (global-company-mode))

(use-package flycheck
  :ensure t
  :defer t
  :config
  (use-package flycheck-irony
    :ensure t
    :defer t
    :init
    (add-hook 'flycheck-mode-hook #'flycheck-irony-setup))
  :init
  (add-hook
   'c++-mode-hook
   (lambda ()
     (setq flycheck-gcc-language-standard "c++11")
     (flycheck-mode))))

(use-package yasnippet
  :ensure t
  :defer 3
  :diminish yas-minor-mode
  :config
  (use-package yasnippet-snippets
    :ensure t)
  (yas-reload-all)
  :init
  (add-hook 'c++-mode-hook 'yas-minor-mode)
  (add-hook 'rust-mode-hook 'yas-minor-mode))

(use-package ranger
  :ensure t
  :defer 2)

(use-package helm
  :ensure t
  :defer 1
  :bind (("M-x" . helm-M-x)
	 ("C-S-b" . helm-buffers-list)))
(use-package ag
  :ensure t
  :defer 2)

(use-package helm-ag
  :defer 2
  :ensure t)

(use-package projectile
  :ensure t
  :defer t
  :commands
  (projectile-ack
   projectile-ag
   projectile-compile-project
   projectile-dired
   projectile-find-dir
   projectile-find-file
   projectile-find-tag
   projectile-test-project
   projectile-grep
   projectile-invalidate-cache
   projectile-kill-buffer
   projectile-project-p
   projectile-project-root
   projectile-recentf
   projectile-regenerate-tags
   projectile-replace
   projectile-replace-regexp
   projectile-run-async-shell-command-in-root
   projectile-run-shell-command-in-root
   projectile-switch-project
   projectile-switch-to-buffer
   projectile-vc)
  :bind (("C-S-p" . helm-projectile-find-file))
  :config
  (setq
   projectile-globally-ignored-directories
   (append '(".git" "target" "build") projectile-globally-ignored-directories)
   projectile-enable-caching t)
  (projectile-global-mode))

(use-package helm-projectile
  :ensure t
  :defer t
  :init
  (helm-projectile-on))

(use-package swiper-helm
  :ensure t
  :defer t)

(use-package rainbow-mode
  :ensure t
  :defer t
  :init
  (rainbow-mode))

(use-package which-key
  :ensure t
  :diminish which-key-mode
  :demand
  :init
  (setq which-key-idle-delay 0.1)
  (setq which-key-popup-type 'minibuffer)
  :config
  (which-key-mode 1))

(use-package rust-mode
  :ensure t
  :defer t)

(use-package json-mode
  :ensure t
  :defer t)

(use-package org
  :mode (("\\.org$" . org-mode))
  :ensure t
  :config
  (progn
    (setq org-log-done 'time)
    (setq org-log-reschedule 'note)
    (setq org-log-into-drawer 'LOGBOOK)

    ;; Refile
    (setq org-refile-targets '((org-agenda-files :maxlevel . 3))) ;; all agenda files; 3 lvls deep
    (setq org-refile-use-outline-path t)
    (setq org-refile-allow-creating-parent-nodes 'confirm)        ;; allow creation of new parent nodes


    (setq org-expiry-inactive-timestamps t)

    ;; Clock
    (setq org-clock-idle-time nil)
    (setq org-clock-continuously nil)
    (setq org-clock-persist t)

    (setq org-columns-default-format "%14SCHEDULED %Effort{:} %1PRIORITY %TODO %50ITEM %TAGS")
    (setq org-export-backends '(org latex icalendar html ascii))
    (setq org-directory "~/org")
    (setq org-default-notes-file "~/org/inbox.org")

    ;; Agenda
    (setq org-agenda-span 14)
    (setq org-agenda-tags-column -100) ; take advantage of the screen width
    (setq org-agenda-sticky nil)
    (setq org-agenda-inhibit-startup t)
    (setq org-agenda-use-tag-inheritance t)
    (setq org-agenda-show-log t)
    (setq org-agenda-skip-scheduled-if-done t)
    (setq org-agenda-skip-deadline-if-done t)
    (setq org-agenda-skip-deadline-prewarning-if-scheduled 'pre-scheduled)
    (setq org-agenda-time-grid
          '((daily today require-timed)
            "----------------"
            (800 1000 1200 1400 1600 1800)))
    (setq org-agenda-files
          (delq nil
                (mapcar (lambda (x) (and x (file-exists-p x) x))
                        `("~/org/private.org"
                          "~/org/work.org"
                          "~/org/inbox.org"))))
    ;; Capture
    (setq org-capture-templates
          '(("t" "ToDo Entry" entry (file+headline "~/org/inbox.org" "Capture") (file "~/org/tpl-todo.txt") :empty-lines-before 1)))

    ;; Keywords
    (setq org-todo-keywords
          '((sequence
             "TODO(t)"  ; next action
             "STARTED(s)"
             "WAITING(w@/!)"
             "SOMEDAY(.)"
             "|"
             "DONE(x!)"
             "CANCELLED(c@)")
            (sequence "BUG" "|" "FIXED(x) WONTFIX")
            (sequence "TODELEGATE(-)" "DELEGATED(d)" "|" "COMPLETE(x)")))

    (setq org-todo-keyword-faces
          '(("TODO" . (:foreground "red" :weight bold))
            ("BUG" . (:foreground "red" :weight bold))
            ("DONE" . (:foreground "green" :weight bold))
            ("FIXED" . (:foreground "green" :weight bold))
            ("COMPLETE" . (:foreground "green" :weight bold))
            ("TODELEGATE" . (:foreground "light blue" :weight bold))
            ("DELEGATED" . (:foreground "dark blue" :weight bold))
            ("WONTFIX" . (:foreground "dark green" :weight bold))
            ("CANCELLED" . (:foreground "dark green" :weight bold))
            ("ONGOING" . (:foreground "orange" :weight bold))
            ("WAITING" . (:foreground "yellow" :weight bold))
            ("SOMEDAY" . (:foreground "gray" :weight bold))))

    (setq org-structure-template-alist ;; fast insert <s[tab]
          '(("s" "#+begin_src ?\n\n#+end_src" "<src lang=\"?\">\n\n</src>")
            ("e" "#+begin_example\n?\n#+end_example" "<example>\n?\n</example>")
            ("q" "#+begin_quote\n?\n#+end_quote" "<quote>\n?\n</quote>")
            ("v" "#+BEGIN_VERSE\n?\n#+END_VERSE" "<verse>\n?\n</verse>")
            ("c" "#+BEGIN_COMMENT\n?\n#+END_COMMENT")
            ("p" "#+BEGIN_PRACTICE\n?\n#+END_PRACTICE")
            ("l" "#+begin_src emacs-lisp\n?\n#+end_src" "<src lang=\"emacs-lisp\">\n?\n</src>")
            ("L" "#+latex: " "<literal style=\"latex\">?</literal>")
            ("h" "#+begin_html\n?\n#+end_html" "<literal style=\"html\">\n?\n</literal>")
            ("H" "#+html: " "<literal style=\"html\">?</literal>")
            ("a" "#+begin_ascii\n?\n#+end_ascii")
            ("A" "#+ascii: ")
            ("i" "#+index: ?" "#+index: ?")
            ("I" "#+include %file ?" "<include file=%file markup=\"?\">")))
    ))

(use-package org-bullets
  :ensure t
  :defer t
  :commands (org-bullets-mode)
  :init (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1))))

(use-package general
  :ensure t
  :config
  (general-define-key
   :states '(normal visual insert emacs)
   :prefix "SPC"
   :non-normal-prefix "C-SPC"

   ;; simple command
   "/"   'counsel-ag
   "TAB" '(switch-to-other-buffer :which-key "prev buffer")
   "SPC" '(avy-goto-word-or-subword-1  :which-key "go to char")

   ;; Coding
   "c"  '(:ignore t :which-key "Coding")
   "cC" '(projectile-compile-project :which-key "Compile Command")
   "cc" '(recompile :which-key "Compile")
   "ch" '(projectile-find-other-file :which-key "Jump hdr/src")
   "cf" '(helm-imenu :which-key "Jump to outline")

   ;; Projectile
   "p"  '(:ignore t :which-key "Projectile")
   "ps" '(helm-projectile-ag :which-key "Find string")
   "pp" '(helm-projectile :which-key "Find file")
   "ph" '(projectile-find-other-file :which-key "Jump hdr/src [deprecated]")

   ;; Buffers
   "b"  '(:ignore t :which-key "Buffers")
   "bb" '(helm-buffers-list :which-key "Find buffer")
   "bs" '(swiper-helm :which-key "Swiper")

   ;; Applications
   "a" '(:ignore t :which-key "Applications")
   "ar" 'ranger

   ;; Git
   "g"  '(:ignore t :which-key "Version Control")
   "gg" '(magit-status :which-key "Magit")
   "gl" '(magit-log-buffer-file :which-key "log")

   ;; Org
   "o"    '(:ignore t :which-key "Org-Mode")
   "oA"   '(org-archive-subtree :which-key "Archive")
   "oa"   '(org-agenda :which-key "Agenda")
   "on"   '(org-narrow-to-subtree :which-key "Narrow")
   "ow"   '(widen :which-key "Widen")
   "oc"   '(:ignore t :which-key "clock")
   "oct"  '(org-clock-goto-may-find-recent-task t :which-key "goto")
   "oci"  '(org-clock-in t :which-key "in")
   "occ"  '(org-clock-last t :which-key "continue")
   "oco"  '(org-clock-out t :which-key "out")

   ;; Toggle
   "t" '(:ignore t :which-key "Toggle")
   "tw" '(whitespace-mode :which-key "Whitespace")
   "tn" '(linum-mode :which-key "Line numbers")
   "tl" '(toggle-truncate-lines :which-key "Line wraps")
   ))

;;; Fin ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(setq gc-cons-threshold (* 2 1000 1000))
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
